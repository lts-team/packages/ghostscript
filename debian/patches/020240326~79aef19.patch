From: Ken Sharp <Ken.Sharp@artifex.com>
Date: Tue, 26 Mar 2024 12:00:14 +0000
Subject: Bug #707686
Origin: https://git.ghostscript.com/?p=ghostpdl.git;a=commit;h=79aef19c685984dc3da2dc090450407d9fbcff80
Bug: https://bugs.ghostscript.com/show_bug.cgi?id=707686
Bug-Debian-Security: https://security-tracker.debian.org/tracker/CVE-2024-33870

See bug thread for details

In addition to the noted bug; an error path (return from
gp_file_name_reduce not successful) could elad to a memory leak as we
did not free 'bufferfull'. Fix that too.

This addresses CVE-2024-33870
---
 base/gpmisc.c | 36 ++++++++++++++++++++++++++++++++----
 1 file changed, 32 insertions(+), 4 deletions(-)

--- a/base/gpmisc.c
+++ b/base/gpmisc.c
@@ -1042,7 +1042,7 @@ gp_validate_path_len(const gs_memory_t *
                      const uint         len,
                      const char        *mode)
 {
-    char *buffer, *bufferfull;
+    char *buffer, *bufferfull = NULL;
     uint rlen;
     int code = 0;
     const char *cdirstr = gp_file_name_current();
@@ -1116,8 +1116,10 @@ gp_validate_path_len(const gs_memory_t *
             return gs_error_VMerror;
 
         buffer = bufferfull + prefix_len;
-        if (gp_file_name_reduce(path, (uint)len, buffer, &rlen) != gp_combine_success)
-            return gs_error_invalidfileaccess;
+        if (gp_file_name_reduce(path, (uint)len, buffer, &rlen) != gp_combine_success) {
+            code = gs_note_error(gs_error_invalidfileaccess);
+            goto exit;
+        }
         buffer[rlen] = 0;
     }
     while (1) {
@@ -1152,9 +1154,34 @@ gp_validate_path_len(const gs_memory_t *
             code = gs_note_error(gs_error_invalidfileaccess);
         }
         if (code < 0 && prefix_len > 0 && buffer > bufferfull) {
+            uint newlen = rlen + cdirstrl + dirsepstrl;
+            char *newbuffer;
+            int code;
+
             buffer = bufferfull;
             memcpy(buffer, cdirstr, cdirstrl);
             memcpy(buffer + cdirstrl, dirsepstr, dirsepstrl);
+
+            /* We've prepended a './' or similar for the current working directory. We need
+             * to execute file_name_reduce on that, to eliminate any '../' or similar from
+             * the (new) full path.
+             */
+            newbuffer = (char *)gs_alloc_bytes(mem->thread_safe_memory, newlen + 1, "gp_validate_path");
+            if (newbuffer == NULL) {
+                code = gs_note_error(gs_error_VMerror);
+                goto exit;
+            }
+
+            memcpy(newbuffer, buffer, rlen + cdirstrl + dirsepstrl);
+            newbuffer[newlen] = 0x00;
+
+            code = gp_file_name_reduce(newbuffer, (uint)newlen, buffer, &newlen);
+            gs_free_object(mem->thread_safe_memory, newbuffer, "gp_validate_path");
+            if (code != gp_combine_success) {
+                code = gs_note_error(gs_error_invalidfileaccess);
+                goto exit;
+            }
+
             continue;
         }
         else if (code < 0 && cdirstrl > 0 && prefix_len == 0 && buffer == bufferfull
@@ -1173,6 +1200,7 @@ gp_validate_path_len(const gs_memory_t *
                                            gs_path_control_flag_is_scratch_file);
     }
 
+exit:
     gs_free_object(mem->thread_safe_memory, bufferfull, "gp_validate_path");
 #ifdef EACCES
     if (code == gs_error_invalidfileaccess)
