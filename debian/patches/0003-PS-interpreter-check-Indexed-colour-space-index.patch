From 4bf18ec34567bf0e85894d62321b008e52933c51 Mon Sep 17 00:00:00 2001
From: Zdenek Hutyra <zhutyra@centrum.cz>
Date: Fri, 30 Aug 2024 13:11:53 +0100
Subject: PS interpreter - check Indexed colour space index

Bug #707990 "Out of bounds read when reading color in "Indexed" color space"

Check the 'index' is in the valid range (0 to hival) for the colour
space.

Also a couple of additional checks on the type of the 'proc' for
Indexed, DeviceN and Separation spaces. Make sure these really are
procs in case the user changed the colour space array.

CVE-2024-46955
---
 psi/zcolor.c | 6 ++++++
 1 file changed, 6 insertions(+)

diff --git a/psi/zcolor.c b/psi/zcolor.c
index 15338cba4..3f3adba46 100644
--- a/psi/zcolor.c
+++ b/psi/zcolor.c
@@ -3628,6 +3628,7 @@ static int septransform(i_ctx_t *i_ctx_p, ref *sepspace, int *usealternate, int
         code = array_get(imemory, sepspace, 3, &proc);
         if (code < 0)
             return code;
+        check_proc(proc);
         *esp = proc;
         return o_push_estack;
     }
@@ -4449,6 +4450,7 @@ static int devicentransform(i_ctx_t *i_ctx_p, ref *devicenspace, int *usealterna
         code = array_get(imemory, devicenspace, 3, &proc);
         if (code < 0)
             return code;
+        check_proc(proc);
         *esp = proc;
         return o_push_estack;
     }
@@ -4864,6 +4866,7 @@ static int indexedbasecolor(i_ctx_t * i_ctx_p, ref *space, int base, int *stage,
             code = array_get(imemory, space, 3, &proc);
             if (code < 0)
                 return code;
+            check_proc(proc);
             *ep = proc;	/* lookup proc */
             return o_push_estack;
         } else {
@@ -4877,6 +4880,9 @@ static int indexedbasecolor(i_ctx_t * i_ctx_p, ref *space, int base, int *stage,
             if (!r_has_type(op, t_integer))
                 return_error (gs_error_typecheck);
             index = op->value.intval;
+            /* Ensure it is in range. See bug #707990 */
+            if (index < 0 || index > pcs->params.indexed.hival)
+                return_error(gs_error_rangecheck);
             /* And remove it from the stack. */
             pop(1);
             op = osp;
-- 
2.30.2

