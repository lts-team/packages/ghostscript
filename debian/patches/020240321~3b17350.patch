From: Ken Sharp <Ken.Sharp@artifex.com>
Date: Thu, 21 Mar 2024 09:01:15 +0000
Subject: Uniprint device - prevent string configuration changes when SAFER
Origin: https://git.ghostscript.com/?p=ghostpdl.git;a=commit;h=3b1735085ecef20b29e8db3416ab36de93e86d1f
Bug: https://bugs.ghostscript.com/show_bug.cgi?id=707662
Bug-Debian-Security: https://security-tracker.debian.org/tracker/CVE-2024-29510

Bug #707662

We cannot sanitise the string arguments used by the Uniprint device
because they can potentially include anything.

This commit ensures that these strings are locked and cannot be
changed by PostScript once SAFER is activated. Full configuration from
the command line is still possible (see the *.upp files in lib).

This addresses CVE-2024-29510
---
 devices/gdevupd.c | 31 +++++++++++++++++++++++++++++++
 1 file changed, 31 insertions(+)

diff --git a/devices/gdevupd.c b/devices/gdevupd.c
index c9389e7bc76d..016a9260a66b 100644
--- a/devices/gdevupd.c
+++ b/devices/gdevupd.c
@@ -1891,6 +1891,16 @@ out on this copies.
       if(!upd_strings[i]) continue;
       UPD_PARAM_READ(param_read_string,upd_strings[i],value,udev->memory);
       if(0 == code) {
+        if (gs_is_path_control_active(udev->memory)) {
+            if (strings[i].size != value.size)
+              error = gs_error_invalidaccess;
+            else {
+                if (strings[i].data && memcmp(strings[i].data, value.data, strings[i].size) != 0)
+                    error = gs_error_invalidaccess;
+            }
+            if (error < 0)
+                goto exit;
+        }
          if(0 <= error) error |= UPD_PUT_STRINGS;
          UPD_MM_DEL_PARAM(udev->memory, strings[i]);
          if(!value.size) {
@@ -1908,6 +1918,26 @@ out on this copies.
       if(!upd_string_a[i]) continue;
       UPD_PARAM_READ(param_read_string_array,upd_string_a[i],value,udev->memory);
       if(0 == code) {
+          if (gs_is_path_control_active(udev->memory)) {
+              if (string_a[i].size != value.size)
+                  error = gs_error_invalidaccess;
+              else {
+                  int loop;
+                  for (loop = 0;loop < string_a[i].size;loop++) {
+                      gs_param_string *tmp1 = (gs_param_string *)&(string_a[i].data[loop]);
+                      gs_param_string *tmp2 = (gs_param_string *)&value.data[loop];
+
+                      if (tmp1->size != tmp2->size)
+                          error = gs_error_invalidaccess;
+                      else {
+                          if (tmp1->data && memcmp(tmp1->data, tmp2->data, tmp1->size) != 0)
+                              error = gs_error_invalidaccess;
+                      }
+                  }
+              }
+            if (error < 0)
+                goto exit;
+          }
          if(0 <= error) error |= UPD_PUT_STRING_A;
          UPD_MM_DEL_APARAM(udev->memory, string_a[i]);
          if(!value.size) {
@@ -2102,6 +2132,7 @@ transferred into the device-structure. In the case of "uniprint", this may
       if(0 > code) error = code;
    }
 
+exit:
    if(0 < error) { /* Actually something loaded without error */
 
       if(!(upd = udev->upd)) {
-- 
2.43.0

