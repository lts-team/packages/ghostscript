From: Ken Sharp <ken.sharp@artifex.com>
Date: Fri, 24 Mar 2023 13:19:57 +0000
Subject: Graphics library - prevent buffer overrun in (T)BCP encoding
Origin: https://git.ghostscript.com/?p=ghostpdl.git;h=37ed5022cecd584de868933b5b60da2e995b3179
Bug-Debian: https://bugs.debian.org/1033757
Bug: https://bugs.ghostscript.com/show_bug.cgi?id=706494
Bug-Debian-Security: https://security-tracker.debian.org/tracker/CVE-2023-28879

Bug #706494 "Buffer Overflow in s_xBCPE_process"

As described in detail in the bug report, if the write buffer is filled
to one byte less than full, and we then try to write an escaped
character, we overrun the buffer because we don't check before
writing two bytes to it.

This just checks if we have two bytes before starting to write an
escaped character and exits if we don't (replacing the consumed byte
of the input).

Up for further discussion; why do we even permit a BCP encoding filter
anyway ? I think we should remove this, at least when SAFER is true.
---
 base/sbcp.c | 10 +++++++++-
 1 file changed, 9 insertions(+), 1 deletion(-)

diff --git a/base/sbcp.c b/base/sbcp.c
index 979ae0992b54..47fc233ec641 100644
--- a/base/sbcp.c
+++ b/base/sbcp.c
@@ -50,6 +50,14 @@ s_xBCPE_process(stream_state * st, stream_cursor_read * pr,
         byte ch = *++p;
 
         if (ch <= 31 && escaped[ch]) {
+            /* Make sure we have space to store two characters in the write buffer,
+             * if we don't then exit without consuming the input character, we'll process
+             * that on the next time round.
+             */
+            if (pw->limit - q < 2) {
+                p--;
+                break;
+            }
             if (p == rlimit) {
                 p--;
                 break;
-- 
2.40.0

